----------------------------------------------------------------------------------------------------
-- SeasonsMeasurementDataEvent
----------------------------------------------------------------------------------------------------
-- Purpose:  Event to ask for measurement and to receive new data.
--           Only contains extra data
--
-- Copyright (c) Realismus Modding, 2019
----------------------------------------------------------------------------------------------------

SeasonsMeasurementDataEvent = {}
local SeasonsMeasurementDataEvent_mt = Class(SeasonsMeasurementDataEvent, Event)

InitEventClass(SeasonsMeasurementDataEvent, "SeasonsMeasurementDataEvent")

function SeasonsMeasurementDataEvent:emptyNew()
    local self = Event:new(SeasonsMeasurementDataEvent_mt)
    return self
end

function SeasonsMeasurementDataEvent:new(measuredObject, baleFermentation)
    local self = SeasonsMeasurementDataEvent:emptyNew()

    self.measuredObject = measuredObject
    self.baleFermentation = baleFermentation

    return self
end

function SeasonsMeasurementDataEvent:writeStream(streamId, connection)
    NetworkUtil.writeNodeObject(streamId, self.measuredObject)

    if self.baleFermentation ~= nil then
        streamWriteBool(streamId, true)
        streamWriteFloat32(streamId, self.baleFermentation)
    else
        streamWriteBool(streamId, false)
    end

end

function SeasonsMeasurementDataEvent:readStream(streamId, connection)
    self.measuredObject = NetworkUtil.readNodeObject(streamId)

    if streamReadBool(streamId) then
        self.baleFermentation = streamReadFloat32(streamId)
    end

    self:run(connection)
end

function SeasonsMeasurementDataEvent:run(connection)
    if connection:getIsServer() then
        MeasureTool.onReceiveExtraMeasureData(self.measuredObject, self.baleFermentation)
    end
end
